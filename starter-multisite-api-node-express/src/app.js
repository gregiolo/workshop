const path = require('path');
const fs   = require('fs');
const express = require('express');

require('express-di');

const Globals = require('./services/Globals');

let _globals = new Globals((__dirname));

class App {

    constructor(app_path, hostname)
    {
        this._path = app_path;

        this._hostname = hostname;

        this._hosts = require(path.join(app_path, 'config/', 'hosts.json'));

        this._app = express();

        var favicon = require('static-favicon');
        var logger = require('morgan');
        var cookieParser = require('cookie-parser');
        var bodyParser = require('body-parser');

        var routes = require('./routes/index');
        var users = require('./routes/users');

/*
        app.use(favicon(), {});
        app.use(logger('dev'), {});
        app.use(bodyParser.json(), {});
        app.use(bodyParser.urlencoded(), {});
        app.use(cookieParser(), {});
        app.use(express.static(path.join(__dirname, 'public')), {});
*/
        this._app.use('/', routes);
        this._app.use('/users', users);

    }


    load() {

        if (this._hosts && this._hosts.length > 0)
        {
            const bootstrap_path = path.join(__dirname, 'sites/Bootstrap.js');

            const Bootstrap = require(bootstrap_path);

            let _site = new Bootstrap(this._app, this._path, this._hosts);

            _site.process();
        }

        this.errors();
    }


    get_app() {
        return this._app;
    }


    errors() {

        this._app.use(function(req, res, next) {
            const err = new Error('Not Found');
            err.status = 404;
            next(err);
        });


        if (this._app.get('env') === 'development') {
            this._app.use(function(err, req, res, next) {
                res.status(err.status || 500);
            });
        }

        this._app.use(function(err, req, res, next) {
            res.status(err.status || 500);
            res.send('respond with a resource');
        });
    }

}

const _app = new App(__dirname, hostname);

_app.load();

module.exports = _app.get_app();
