## Workshop

**Private workshop for MERN stack. You can try to find something useful.**

1. **starter-typescript-react-webpack-ts**: premade starter kit for application that relies on: TypeScript, React, Webpack, TS. Project with .tsx files and ES6, using react v16 without `typings` manager but @types/

2. **starter-multisite-api-node-express**: a starter kit that supports multiple hosts coming from nginx proxy as x-proxy-header, then check privileges in DB (Mongo) for access particular resource and response
